def contains_blue_devil(filename='example_text.txt'):
    f = open('example_text.txt', 'r')
    lines = f.readlines()
    f.close()

    found = False
    for line in lines:
        index = line.find('Blue Devil')
        if index != -1:
            found = True
            break

    return found
