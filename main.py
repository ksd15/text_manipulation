
def strip_lines(lines):
    lines_striped = {}
    for i in range(len(lines)):
        curr = lines[i]
        curr_strip = curr.rstrip()
        lines_striped[i] = curr_strip
    return (lines_striped)

if __name__ == "__main__":
    import count_men as cm
    import contains_blue_devil as cbd

    # 2
    filename = 'example_text.txt'
    f = open(filename, 'r')
    print('Output from readline(): ')
    print(f.readline())
    f.close()

    # 2
    fs = open(filename, 'r')
    print('Output from readlines(): ')
    lines = fs.readlines()
    print(lines)
    fs.close()

    # 4
    lines_striped = strip_lines(lines)
    print('Output after striping new-line characters')
    print(lines_striped)

    # 5
    nmen = cm.count_men(filename)
    print('"men" word count: ' + str(nmen))

    # 6
    from capitalize_women import capitalize_women
    capitalize_women(filename = 'example_text.txt')
    
    # 7
    bluedevilflag = cbd.contains_blue_devil(filename)
    print('"Blue Devil" contained in file: ' + str(bluedevilflag))
