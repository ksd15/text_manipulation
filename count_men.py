def count_men(filename='example_text.txt'):
    f = open('example_text.txt', 'r')
    lines = f.readlines()
    f.close()

    nwords = 0
    for line in lines:
        line = line.lower()
        nwords += line.count(' men')

    return nwords
